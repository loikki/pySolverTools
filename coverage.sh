#!/bin/bash

rm .coverage
rm .coverage.*

function test {
    echo $1
    $1
    CODE=$?
    if [[ $CODE -ne 0 ]]; then
	echo $1 " Exit Code " $CODE
	echo "Test failed"
	exit 1
    fi
}

pep8 --version
# need to use gcov for C functions
test "pep8 --config=${PWD}/setup.cfg pysolvertools test"

# Test poisson with non-periodic boundaries
test "python -m coverage run test/testpoisson.py --dim 1 --periodic 0"
test "python -m coverage run test/testpoisson.py --dim 2 --periodic 0"
# low accuracy due to memory limitation of gitlab-ci
test "python -m coverage run test/testpoisson.py --dim 3 --periodic 0 --eps 0.1"

# Test poisson with periodic boundaries
test "python -m coverage run test/testpoisson.py --dim 1"
test "python -m coverage run test/testpoisson.py --dim 2"
test "python -m coverage run test/testpoisson.py --dim 3 --eps 0.1"

# Test deposit with non-periodic boundaries
test "python -m coverage run test/testdeposit.py --dim 1 --periodic 0"
test "python -m coverage run test/testdeposit.py --dim 2 --periodic 0"
test "python -m coverage run test/testdeposit.py --dim 3 --periodic 0 --eps 0.1"

# Test deposit with periodic boundaries
test "python -m coverage run test/testdeposit.py --dim 1"
test "python -m coverage run test/testdeposit.py --dim 2"
test "python -m coverage run test/testdeposit.py --dim 3 --eps 0.1"

python -m coverage combine
python -m coverage report
python -m coverage html
