#ifndef __PROTO_H__
#define __PROTO_H__

#include <Python.h>
#include <numpy/arrayobject.h>

void _poissonSolver1D(PyArrayObject* rho,
		      PyArrayObject* phi,
		      int* n,
		      float h);


void _poissonSolverFourier1D(PyArrayObject* rho,
			     int* n,
			     float* l);

void _poissonSolverFourier2D(PyArrayObject* rho,
			     int* n,
			     float* l);

void _poissonSolverFourier3D(PyArrayObject* rho,
			     int* n,
			     float* l);

void _poissonSolverSSOR1D(PyArrayObject* rho,
			  PyArrayObject* phi,
			  int* n,
			  float* h);
/*
void _poissonSolverSSOR2D(PyArrayObject* rho,
			  PyArrayObject* phi,
			  int* n,
			  float* h);

void _poissonSolverSSOR3D(PyArrayObject* rho,
			  PyArrayObject* phi,
			  int* n,
			  float* h);
*/

PyArrayObject* _poissonSolver(PyArrayObject* rho,
			      int* n,
			      float* l,
			      int periodic);

PyArrayObject* _deposit(PyArrayObject* pos,
			PyArrayObject* weight,
			PyArrayObject* rho,
			float* ori,
			float* l,
			npy_intp* n,
			int periodic);

void _deposit1D(PyArrayObject* rho,
		PyArrayObject* pos,
		PyArrayObject* weight,
		float* ori,
		float* l,
		npy_intp* n,
		int periodic);

void _deposit2D(PyArrayObject* rho,
		PyArrayObject* pos,
		PyArrayObject* weight,
		float* ori,
		float* l,
		npy_intp* n,
		int periodic);

void _deposit3D(PyArrayObject* rho,
		PyArrayObject* pos,
		PyArrayObject* weight,
		float* ori,
		float* l,
		npy_intp* n,
		int periodic);

#endif //__PROTO_H__
