[![build status](https://gitlab.com/loikki/pySolverTools/badges/master/build.svg)](https://gitlab.com/loikki/pySolverTools/commits/master)
[![coverage report](https://gitlab.com/loikki/pySolverTools/badges/master/coverage.svg)](https://gitlab.com/loikki/pySolverTools/commits/master)

# TODO
Need to allow 1D array for poisson equation (e.g. shape: (N,) not only (N,1)) => modify test

# pySolverTools
Fast python PDE equation solver.

Currently solves only the poisson equation ($`\Delta \phi = f`$) with FFT in C.

The final project should be in C/CUDA and gives three different solvers: GPU, CPU and MPI/OMP.


Example
-------
``` python
import numpy as np
import pysolvertools

# System size
L = [1.0, 1.0, 1.0]
# Mesh size
N = [256, 256, 256]
# number of particles
Np = 1e6
# periodic boundaries
periodic = 1
# Origin of the system
# move the box from [0, 1.0]^3
# to [-0.5, 0.5]^3
O = [-0.5, -0.5, -0.5]
# particles position
pos = np.array(
    np.random.normal(loc=0, scale=0.1, size=(Np,3)),
    dtype=np.float32)
# weight (e.g. charge, mass)
weight = np.ones(Np)


# compute potential in two steps
# first weight deposit
rho = pysolvertools.deposit(pos, weight, L, N, O, periodic)
# then poisson solver
pot1 = pysolvertools.poissonSolver(rho, L, periodic)

# same as before but hide the two steps
pot = pysolvertools.poissonSolverWithDeposit(pos, weight, L, N, O, periodic)
pot == pot1 # True

# compute the potential at the particles' position
pot_pos = pysolvertools.getPotential(pot,pos,L,O,periodic)

# same as before but directly from the particles
pot_pos = pysolvertools.getPotentialFromDeposit(pos, weight, L, N, O, periodic)

```

Install
-------

A basic installation is done in the following way.
``` bash
cd src
python setup.py build
sudo python setup.py install
```

Test
----

The following script can be used to test the code.
``` bash
./coverage.sh
```